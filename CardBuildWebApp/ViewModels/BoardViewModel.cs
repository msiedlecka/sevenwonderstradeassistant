﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using CardBuildWebApp.Models;
using ExtensionMethods;

namespace CardBuildWebApp.ViewModels
{
    public class BoardViewModel : Editable, IProcessChanges
    {
        public BoardViewModel(Boards board, int index)
        {
            PlayerBoard = board;
            PlayerIndex = index;
            AvailableBoards = new List<Boards>(Enum.GetValues(typeof(Boards)).Cast<Boards>());
            AvailableBoards.Remove(board);
            AvailableAsSelectItems = new List<SelectListItem>();
            RefreshAvailableAsSelectItems();
        }

        public BoardViewModel() : this(Boards.board_alexandria, 1)
        {
        }

        public int PlayerIndex { get; set; }

        [Display(Name = "Plansza gracza")]
        public Boards PlayerBoard { get; set; }

        public Boards NewPlayerBoard { get; set; }

        // contains all the boards except the one currently chosen by the player
        [Display(Name = "Dostępne plansze")]
        public List<Boards> AvailableBoards { get; set; }

        public List<SelectListItem> AvailableAsSelectItems { get; set; }

        public void ProcessChanges()
        {
            SetBoard(NewPlayerBoard);
        }

        public void SetBoard(Boards newBoard)
        {
            AvailableBoards.Add(PlayerBoard);
            PlayerBoard = newBoard;
            AvailableBoards.Remove(newBoard);
            RefreshAvailableAsSelectItems();
        }

        private void RefreshAvailableAsSelectItems()
        {
            AvailableAsSelectItems.Clear();
            AvailableAsSelectItems = AvailableBoards.Select(item => new SelectListItem
            {
                Text = item.DisplayName(),
                Value = item.ToString()
            }).ToList();
        }

        protected override void SetEditModeForChildren(bool isEditMode)
        {
        }
    }
}