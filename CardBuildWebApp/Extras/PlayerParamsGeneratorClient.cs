﻿using System;
using System.Collections.Generic;
using CardBuildWebApp.Models;
using CardBuildWebApp.PlayerParamsCreator;

namespace CardBuildWebApp.Extras
{
    public static class PlayerParamsGeneratorClient
    {
        private static PlayerParamsCreatorClient client = new PlayerParamsCreatorClient();

        static PlayerParamsGeneratorClient()
        {
            client.InnerChannel.OperationTimeout = new TimeSpan(0, 0, 2);
        }

        public static Boards GetRandomBoard()
        {
            Boards randomBoard;
            try
            {
                string response = client.GetRandomBoard("", "");
                EnumConverter<Boards> conv = new EnumConverter<Boards>();
                randomBoard = conv.ConvertFrom(response);
            }
            catch
            {
                randomBoard = Boards.board_alexandria;
            }
            return randomBoard;
        }

        public static int GetRandomCoins()
        {
            int response;
            try
            {
                response = client.GetRandomCoins();
            }
            catch
            {
                response = 3;
            }
            return response;
        }

        public static List<Merchandise> GetRandomMerchandise()
        {
            List<Merchandise> randomMerchandise = new List<Merchandise>();
            try
            {
                string[] response = client.GetRandomMerchandise();

                EnumConverter<Merchandise> conv = new EnumConverter<Merchandise>();
                if (response != null)
                    foreach (string name in response)
                        randomMerchandise.Add(conv.ConvertFrom(name));
            }
            catch
            {
            }
            return randomMerchandise;
        }

        public static List<MerchandiseAlternative> GetRandomAlternative()
        {
            List<MerchandiseAlternative> randomAlternative = new List<MerchandiseAlternative>();
            try
            {
                string[] response = client.GetRandomMerchandiseAlts(AlternativeConverter.Delimiter);

                AlternativeConverter conv = new AlternativeConverter();
                if (response != null)
                    foreach (string name in response)
                        randomAlternative.Add(conv.ConvertFrom(name,
                            EnumConverter<Merchandise>.MapStringValuesToObjects()));
            }
            catch
            {
            }
            return randomAlternative;
        }

        public static List<TradeBonuses> GetRandomTradeBonuses()
        {
            List<TradeBonuses> randomBonuses = new List<TradeBonuses>();
            try
            {
                string[] response = client.GetRandomTradeBonuses();

                EnumConverter<TradeBonuses> conv = new EnumConverter<TradeBonuses>();
                if (response != null)
                    foreach (string name in response)
                        randomBonuses.Add(conv.ConvertFrom(name));
            }
            catch
            {
            }
            return randomBonuses;
        }
    }
}