﻿using System.Collections.Generic;
using CardBuildWebApp.Models;

namespace CardBuildWebApp.ViewModels
{
    public class MerchandiseAlternativeViewModel : ListChooserViewModel<MerchandiseAlternative>
    {
        public MerchandiseAlternativeViewModel() : base(false, 1)
        {
        }

        public MerchandiseAlternativeViewModel(int playerIndex, params MerchandiseAlternative[] alternatives)
            : base(false, playerIndex, alternatives)
        {
        }

        public override IEnumerable<MerchandiseAlternative> GetAllAvailableValues()
        {
            return new List<MerchandiseAlternative>
            {
                new MerchandiseAlternative(Merchandise.merchandise_wood, Merchandise.merchandise_stone),
                new MerchandiseAlternative(Merchandise.merchandise_wood, Merchandise.merchandise_clay),
                new MerchandiseAlternative(Merchandise.merchandise_wood, Merchandise.merchandise_iron_ore),
                new MerchandiseAlternative(Merchandise.merchandise_stone, Merchandise.merchandise_clay),
                new MerchandiseAlternative(Merchandise.merchandise_stone, Merchandise.merchandise_iron_ore),
                new MerchandiseAlternative(Merchandise.merchandise_clay, Merchandise.merchandise_iron_ore),
                new MerchandiseAlternative(Merchandise.merchandise_papyrus, Merchandise.merchandise_glass,
                    Merchandise.merchandise_cloth),
                new MerchandiseAlternative(Merchandise.merchandise_wood, Merchandise.merchandise_stone,
                    Merchandise.merchandise_clay, Merchandise.merchandise_iron_ore)
            };
        }

        protected override MerchandiseAlternative GetDefaultItemToAdd()
        {
            return new MerchandiseAlternative(Merchandise.merchandise_wood, Merchandise.merchandise_clay);
        }
    }
}