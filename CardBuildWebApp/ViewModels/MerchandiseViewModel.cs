﻿using System;
using System.Collections.Generic;
using System.Linq;
using CardBuildWebApp.Models;

namespace CardBuildWebApp.ViewModels
{
    public class MerchandiseViewModel : ListChooserViewModel<Merchandise>
    {
        public MerchandiseViewModel() : base(true, 1)
        {
        }

        public MerchandiseViewModel(int playerIndex, params Merchandise[] merchandises)
            : base(true, playerIndex, merchandises)
        {
        }

        public override IEnumerable<Merchandise> GetAllAvailableValues()
        {
            return Enum.GetValues(typeof(Merchandise)).Cast<Merchandise>();
        }

        protected override Merchandise GetDefaultItemToAdd()
        {
            return Merchandise.merchandise_wood;
        }
    }
}