﻿using System.ComponentModel.DataAnnotations;

namespace CardBuildWebApp.Models
{
    public enum TradeBonuses
    {
        [Display(Name = "Surowce za 1 monetę z prawym sąsiadem")] bonus_trade_raw_materials_right,
        [Display(Name = "Surowce za 1 monetę z lewym sąsiadem")] bonus_trade_raw_materials_left,
        [Display(Name = "Surowce za 1 monetę z oboma sąsiadami")] bonus_trade_raw_materials_both,
        [Display(Name = "Dobra za 1 monetę z oboma sąsiadami")] bonus_trade_manufactured_goods_both
    }
}