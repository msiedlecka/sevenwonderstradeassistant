﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using CardBuildWebApp.Models;
using ExtensionMethods;

namespace CardBuildWebApp.Extras
{
    public class MerchandiseAlternativeConverter : TypeConverter
    {
        private static readonly string delimiter = "/";

        // from string (to MerchandiseAlternative)
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;

            return base.CanConvertFrom(context, sourceType);
        }

        // from string (to MerchandiseAlternative)
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                string textValue = value as string;
                AlternativeConverter conv = new AlternativeConverter();
                return conv.ConvertFrom(textValue, EnumConverter<Merchandise>.MapLabelsToObjects());
            }

            return base.ConvertFrom(context, culture, value);
        }

        // to string (from MerchandiseAlternative)
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
                return true;

            return base.CanConvertTo(context, destinationType);
        }

        // to string (from MerchandiseAlternative)
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value,
            Type destinationType)
        {
            if (destinationType == typeof(string) && value is MerchandiseAlternative)
            {
                MerchandiseAlternative alternative = value as MerchandiseAlternative;
                StringBuilder sb = new StringBuilder();
                alternative.Merchandise.ForEach(m => sb.AppendFormat("{0}{1}", m.DisplayName(), delimiter));
                if (sb.Length > 0)
                    sb.Remove(sb.Length - 1, 1); // remove the last delimiter
                return sb.ToString();
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override bool IsValid(ITypeDescriptorContext context, object value)
        {
            return value is MerchandiseAlternative;
        }
    }
}