﻿using System.Collections.Generic;

namespace CardBuildWebApp.ViewModels
{
    public class TotalViewModel : Editable, IProcessChanges
    {
        public TotalViewModel(bool isEditMode)
        {
            Players = new List<PlayerViewModel>
            {
                new PlayerViewModel("left", 0),
                new PlayerViewModel("main", 1),
                new PlayerViewModel("right", 2)
            };
            RequiredMerchandises = new RequiredMerchandiseViewModel();
            SetEditMode(false);
        }

        public TotalViewModel() : this(false)
        {
        }

        public List<PlayerViewModel> Players { get; set; }

        public RequiredMerchandiseViewModel RequiredMerchandises { get; set; }

        public void ProcessChanges()
        {
            Players.ForEach(player => player.ProcessChanges());
            RequiredMerchandises.ProcessChanges();
        }

        protected override void SetEditModeForChildren(bool isEditMode)
        {
            Players.ForEach(playerVM => playerVM.SetEditMode(isEditMode));
            RequiredMerchandises.SetEditMode(isEditMode);
        }
    }
}