﻿using System.ComponentModel.DataAnnotations;
using CardBuildWebApp.CardBuildServiceRef;
using CardBuildWebApp.Models;

namespace CardBuildWebApp.ViewModels
{
    public class RequiredMerchandiseViewModel : MerchandiseViewModel
    {
        public RequiredMerchandiseViewModel()
        {
            BuildPossibility = BuildPossibilityType.NOT_DETERMINED;
        }

        public RequiredMerchandiseViewModel(params Merchandise[] merchandises) : base(1, merchandises)
        {
            BuildPossibility = BuildPossibilityType.NOT_DETERMINED;
        }

        [Display(Name = "Nazwa karty")]
        public string CardName { get; set; }

        public BuildPossibilityType BuildPossibility { get; set; }

        public override void ProcessChanges()
        {
            base.ProcessChanges();
            BuildPossibility = BuildPossibilityType.NOT_DETERMINED;
        }
    }
}