﻿using System.Collections.Generic;
using System.Linq;
using CardBuildWebApp.CardBuildServiceRef;

namespace CardBuildWebApp.Models
{
    public class Player
    {
        public string Id { get; set; }
        public string NameToDisplay { get; set; }
        public Boards Board { get; set; }
        public int Coins { get; set; }
        public List<TradeBonuses> TradeBonuses { get; set; }
        public List<Merchandise> Merchandise { get; set; }
        public List<MerchandiseAlternative> MerchandiseAlternatives { get; set; }

        public PlayerSimpleData ToPlayerSimpleData()
        {
            PlayerSimpleData playerData = new PlayerSimpleData();
            playerData.Name = Id;
            playerData.LeftPlayerName = playerData.RightPlayerName = "";
            playerData.BoardKey = Board.ToString();
            playerData.Coints = Coins;

            playerData.TradeBonusKeys = TradeBonuses.Select(t => t.ToString()).ToArray();
            playerData.MerchandiseKeys = Merchandise.Select(m => m.ToString()).ToArray();

            List<List<string>> merchandiseAltKeys = new List<List<string>>();
            foreach (MerchandiseAlternative mAlt in MerchandiseAlternatives)
            {
                List<string> merchKeys = new List<string>();
                mAlt.Merchandise.ForEach(m => merchKeys.Add(m.ToString()));
                merchandiseAltKeys.Add(merchKeys);
            }

            playerData.MerchandiseAlternativeKeys = merchandiseAltKeys.Select(l => l.ToArray()).ToArray();
            return playerData;
        }
    }
}