﻿using System;
using System.Collections.Generic;
using CardBuildWebApp.Models;

namespace CardBuildWebApp.Extras
{
    public class AlternativeConverter
    {
        public static readonly string Delimiter = "/";

        public MerchandiseAlternative ConvertFrom(string separatedMerchandise, Dictionary<string, Merchandise> map)
        {
            string[] merchandisesDisplayNames = separatedMerchandise.Split(new[] {Delimiter},
                StringSplitOptions.RemoveEmptyEntries);
            List<Merchandise> merchandises = new List<Merchandise>();
            Merchandise found;
            foreach (string displayName in merchandisesDisplayNames)
                if (map.TryGetValue(displayName, out found))
                    merchandises.Add(found);
            MerchandiseAlternative alternative = new MerchandiseAlternative();
            alternative.Merchandise = merchandises;
            return alternative;
        }
    }
}