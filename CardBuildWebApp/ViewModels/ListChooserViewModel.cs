﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ExtensionMethods;

namespace CardBuildWebApp.ViewModels
{
    public abstract class ListChooserViewModel<T> : Editable, IProcessChanges
    {
        public ListChooserViewModel(bool allowDuplicates, int index)
        {
            AllowDuplicates = allowDuplicates;
            PlayerIndex = index;
            ChosenItems = new List<T>();
            AvailableItems = new List<T>(GetAllAvailableValues());
            AvailableAsSelectItems = new List<SelectListItem>();
            RefreshAvailableAsSelectItems();
            ItemToAdd = GetDefaultItemToAdd();
        }

        public ListChooserViewModel(bool allowDuplicates, int index, params T[] objects) : this(allowDuplicates, index)
        {
            if (objects != null)
                foreach (T obj in objects)
                    Add(obj);
        }

        private bool AllowDuplicates { get; set; }

        public int PlayerIndex { get; set; }

        public List<T> ChosenItems { get; set; }

        public List<T> AvailableItems { get; set; }

        public List<SelectListItem> AvailableAsSelectItems { get; set; }

        public T ItemToAdd { get; set; }

        public virtual void ProcessChanges()
        {
            Add(ItemToAdd);
        }

        public void Clear()
        {
            ChosenItems.Clear();
            AvailableItems.Clear();
            AvailableItems.AddRange(GetAllAvailableValues());
            AvailableAsSelectItems.Clear();
            RefreshAvailableAsSelectItems();
        }

        public abstract IEnumerable<T> GetAllAvailableValues();

        protected abstract T GetDefaultItemToAdd();

        public void Add(T item)
        {
            if (AllowDuplicates || (!AllowDuplicates && !ChosenItems.Contains(ItemToAdd))) ChosenItems.Add(item);

            if (!AllowDuplicates)
            {
                AvailableItems.Remove(item);
                RefreshAvailableAsSelectItems();
            }
        }

        public void Remove(T item)
        {
            ChosenItems.Remove(item);
            if (!AllowDuplicates)
            {
                AvailableItems.Add(item);
                RefreshAvailableAsSelectItems();
            }
        }

        private void RefreshAvailableAsSelectItems()
        {
            AvailableAsSelectItems.Clear();
            AvailableAsSelectItems = AvailableItems.Select(item => new SelectListItem
            {
                Text = item.DisplayName(),
                Value = item.ToString()
            }).ToList();
        }

        protected override void SetEditModeForChildren(bool isEditMode)
        {
        }
    }
}