﻿using System.ComponentModel.DataAnnotations;

namespace CardBuildWebApp.Models
{
    public enum Merchandise
    {
        [Display(Name = "Ruda żelaza")] merchandise_iron_ore,
        [Display(Name = "Glina")] merchandise_clay,
        [Display(Name = "Kamień")] merchandise_stone,
        [Display(Name = "Drewno")] merchandise_wood,
        [Display(Name = "Papirus")] merchandise_papyrus,
        [Display(Name = "Tkanina")] merchandise_cloth,
        [Display(Name = "Szkło")] merchandise_glass
    }
}