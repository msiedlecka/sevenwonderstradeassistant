# 7 Wonders™ Trade Assistant #
Trade Assistant is a services-based web application that helps [7 Wonders™](https://www.boardgamegeek.com/boardgame/68448/7-wonders) board game players to find out whether they have enough resources to buy specific set of merchandise. It takes into account various game bonuses, e.g. discounts for particular types of merchandise when trading with neighbours. In this game trading is especially useful when players wish to build a building they can't afford, so in order to make the app easier to use we included an option to pick a card with a building of interest and the program itself checks what resources are required to build it. *Note:* not all cards have been implemented.

This is a part of an academic project implemented for classes of .NET Internet Systems subject (edition 2015/2016) carried at Gdansk University of Technology, Faculty of Electronics, Telecommunications and Informatics. It is **not maintained**.

Trade Assistant app logic builds up on 3 services (which are the work of [Paulina Rudnicka](https://bitbucket.org/prudnis)). Originally, they were all built in different technologies (Java Web Service, ASP.NET Web Service, WCF Service) and were temporarily deployed on academic servers (due to the teacher's special requirements). But the main subject of this publication is the ASP.NET MVC web app, so to ease the showcase setup they have all been converted to WCF and closed in a separate library. GameServicesHost is a small utility program that starts up those services and hosts them in an executable (so called *self-hosting*). It is possible to configure the addresses of the services.

Prebuilt binaries for GameServicesHost are available in *Downloads* section.

## How do I get set up? ##
Here's what I used to build the project: C#, .NET Framework 4.5.2, ASP.NET MVC 5, Bootstrap, jQuery. In order to get the web application up and running, one needs to:
1) Run GameServicesHost.exe (*note:* you might need administrator priviledges)
2) Deploy CardBuildWebApp on IIS Server.
*Note:* if you change the addresses of the services in GameServicesHost, you need to update the service location information in Service References section of CardBuildWebApp  project.

## Screenshots ##
*Note:* only Polish version available
![All.png](https://bitbucket.org/repo/ypKRRnX/images/3479023195-All.png)

### License ###
MIT/X11 Copyright (c) 2015 Monika Siedlecka, [Paulina Rudnicka](https://bitbucket.org/prudnis)