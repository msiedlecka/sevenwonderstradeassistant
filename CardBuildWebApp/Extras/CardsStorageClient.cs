﻿using System;
using System.Collections.Generic;
using System.Linq;
using CardBuildWebApp.CardInfoStorage;
using CardBuildWebApp.Models;

namespace CardBuildWebApp.Extras
{
    public static class CardsStorageClient
    {
        public static List<string> GetAllCardNames()
        {
            CardInfoStorageClient client = new CardInfoStorageClient();
            string[] result = client.GetAvailableCards();
            return FormatNames(result);
        }

        public static List<Merchandise> GetMerchandiseForCard(string cardNameEntered)
        {
            string cardNameForService = GetOriginalName(cardNameEntered);
            CardInfoStorageClient client = new CardInfoStorageClient();
            string[] result = client.GetMerchandiseForCard(cardNameForService);
            List<Merchandise> merchandiseForCard = new List<Merchandise>();
            EnumConverter<Merchandise> conv = new EnumConverter<Merchandise>();
            if (result != null)
                Array.ForEach(result, merchandiseName => merchandiseForCard.Add(conv.ConvertFrom(merchandiseName)));
            return merchandiseForCard;
        }

        private static List<string> FormatNames(string[] names)
        {
            for (int i = 0; i < names.Length; i++)
                names[i] = FormatName(names[i]);
            return names.ToList();
        }

        private static string FormatName(string name)
        {
            name = name.Replace('_', ' ');
            if (name.Length > 0)
                name = char.ToUpper(name[0]) + (name.Length > 1 ? name.Substring(1) : "");
            return name;
        }

        private static string GetOriginalName(string nameEntered)
        {
            if (nameEntered != null)
            {
                nameEntered = nameEntered.ToLower();
                nameEntered = nameEntered.Replace(' ', '_');
            }
            else nameEntered = "";
            return nameEntered;
        }
    }
}