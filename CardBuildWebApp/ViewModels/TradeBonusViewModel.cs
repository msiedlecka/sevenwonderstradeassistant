﻿using System;
using System.Collections.Generic;
using System.Linq;
using CardBuildWebApp.Models;

namespace CardBuildWebApp.ViewModels
{
    public class TradeBonusViewModel : ListChooserViewModel<TradeBonuses>
    {
        public TradeBonusViewModel() : base(false, 1)
        {
        }

        public TradeBonusViewModel(int playerIndex, params TradeBonuses[] bonuses) : base(false, playerIndex, bonuses)
        {
        }

        public override IEnumerable<TradeBonuses> GetAllAvailableValues()
        {
            return Enum.GetValues(typeof(TradeBonuses)).Cast<TradeBonuses>();
        }

        protected override TradeBonuses GetDefaultItemToAdd()
        {
            return TradeBonuses.bonus_trade_raw_materials_both;
        }
    }
}