﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using CardBuildWebApp.Extras;

namespace CardBuildWebApp.Models
{
    [TypeConverter(typeof(MerchandiseAlternativeConverter))]
    public class MerchandiseAlternative
    {
        public MerchandiseAlternative(params Merchandise[] merchandises)
        {
            Merchandise = new List<Merchandise>();
            if (merchandises != null)
                Merchandise.AddRange(merchandises);
        }

        public List<Merchandise> Merchandise { get; set; }

        public override string ToString()
        {
            TypeConverter converter = TypeDescriptor.GetConverter(GetType());
            return (string) converter.ConvertTo(null, CultureInfo.CurrentCulture, this, typeof(string));
        }
    }
}