﻿namespace CardBuildWebApp.ViewModels
{
    public abstract class Editable
    {
        public bool IsInEditMode { get; set; }

        // sets IsInEditMode = isEditMode for itself and all its Editable fields
        public void SetEditMode(bool isEditMode)
        {
            IsInEditMode = isEditMode;
            SetEditModeForChildren(isEditMode);
        }

        // inheriting classes should set IsInEditMode = isEditMode for all their Editable fields
        protected abstract void SetEditModeForChildren(bool isEditMode);
    }
}