﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using CardBuildLibrary;

namespace GameServicesHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            List<Tuple<Type, Uri>> services = new List<Tuple<Type, Uri>>
            {
                new Tuple<Type, Uri>(typeof(CardBuildService), new Uri("http://localhost:9876/CardBuildService")),
                new Tuple<Type, Uri>(typeof(CardInfoStorage), new Uri("http://localhost:9765/CardInfoStorage")),
                new Tuple<Type, Uri>(typeof(PlayerParamsCreator), new Uri("http://localhost:9654/PlayerParamsCreator"))
            };
            List<ServiceHost> hosts = new List<ServiceHost>();
            bool allStartedSuccessfully = true;

            foreach (var service in services)
            {
                hosts.Add(StartHost(service.Item1, service.Item2));
                if (hosts.Last() == null)
                {
                    allStartedSuccessfully = false;
                    break;
                }
            }

            if (allStartedSuccessfully)
            {
                Console.WriteLine("\nPress any key to close the services");
                Console.ReadKey();
                CloseHosts(hosts);
            }
            else
            {
                CloseHosts(hosts);
            }
        }

        private static void CloseHosts(List<ServiceHost> hosts)
        {
            foreach (var host in hosts)
                if (host != null)
                    host.Close();
        }

        private static ServiceHost StartHost(Type serviceType, Uri location)
        {
            ServiceHost svcHost = new ServiceHost(serviceType);
            try
            {
                svcHost.Open();
                Console.WriteLine("Service is running  at following address: {0}\n", location.OriginalString);
            }
            catch (Exception ex)
            {
                svcHost = null;
                Console.WriteLine("Service {0} can not be started \n\nError Message: {1}\nPress any key to exit",
                    location.OriginalString, ex.Message);
                Console.ReadKey();
            }
            return svcHost;
        }
    }
}