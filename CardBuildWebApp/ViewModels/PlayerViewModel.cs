﻿using System.ComponentModel.DataAnnotations;
using CardBuildWebApp.Models;

namespace CardBuildWebApp.ViewModels
{
    public class PlayerViewModel : Editable, IProcessChanges
    {
        public PlayerViewModel(string id, int index)
        {
            Id = id;
            Index = index;
            Name = "";
            Board = new BoardViewModel();
            Coins = 0;
            Merchandises = new MerchandiseViewModel(Index);
            Alternatives = new MerchandiseAlternativeViewModel(Index);
            Bonuses = new TradeBonusViewModel(Index);
        }

        public PlayerViewModel() : this("main", 1)
        {
        }

        public int Index { get; set; }

        public string Id { get; set; }

        [Display(Name = "Imię gracza")]
        [Required(ErrorMessage = "Wprowadź imię tego gracza.")]
        public string Name { get; set; }

        [Display(Name = "Plansza gracza")]
        public BoardViewModel Board { get; set; }

        [Display(Name = "Liczba monet")]
        [Range(0, 150, ErrorMessage = "Liczba monet musi zawierać się w przedziale od 0 do 150.")]
        [Required(ErrorMessage = "Wprowadź liczbę monet posiadanych przez tego gracza.")]
        public int Coins { get; set; }

        [Display(Name = "Produkowane surowce")]
        public MerchandiseViewModel Merchandises { get; set; }

        [Display(Name = "Surowce do wyboru")]
        public MerchandiseAlternativeViewModel Alternatives { get; set; }

        [Display(Name = "Bonusy handlowe")]
        public TradeBonusViewModel Bonuses { get; set; }

        public void ProcessChanges()
        {
            Board.ProcessChanges();
            Merchandises.ProcessChanges();
            Alternatives.ProcessChanges();
            Bonuses.ProcessChanges();
        }

        public Player ToPlayer()
        {
            Player player = new Player();
            player.Id = Id;
            player.NameToDisplay = Name;
            player.Merchandise = Merchandises.ChosenItems;
            player.MerchandiseAlternatives = Alternatives.ChosenItems;
            player.TradeBonuses = Bonuses.ChosenItems;
            player.Board = Board.PlayerBoard;
            player.Coins = Coins;
            return player;
        }

        protected override void SetEditModeForChildren(bool isEditMode)
        {
            Board.SetEditMode(isEditMode);
            Merchandises.SetEditMode(isEditMode);
            Alternatives.SetEditMode(isEditMode);
            Bonuses.SetEditMode(isEditMode);
        }
    }
}