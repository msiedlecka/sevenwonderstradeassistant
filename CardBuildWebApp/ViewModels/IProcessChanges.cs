﻿namespace CardBuildWebApp.ViewModels
{
    public interface IProcessChanges
    {
        void ProcessChanges();
    }
}