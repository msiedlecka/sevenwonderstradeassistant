﻿using CardBuildWebApp.ViewModels;

namespace CardBuildWebApp.Extras
{
    public static class Utils
    {
        public static string GetPartialViewNameFor(Editable editable, string standardPartialView)
        {
            return standardPartialView + (editable.IsInEditMode ? "_Edit" : "");
        }
    }
}