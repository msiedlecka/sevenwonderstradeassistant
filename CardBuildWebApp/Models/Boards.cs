﻿using System.ComponentModel.DataAnnotations;

namespace CardBuildWebApp.Models
{
    public enum Boards
    {
        [Display(Name = "Latarnia Morska w Aleksandrii")] board_alexandria,
        [Display(Name = "Kolos Rodyjski")] board_collosus,
        [Display(Name = "Świątynia Artemidy w Efezie")] board_artemis,
        [Display(Name = "Wiszące Ogrody Semiramidy")] board_babylon,
        [Display(Name = "Piramidy w Gizie")] board_gizeh,
        [Display(Name = "Mauzoleum w Halikarnasie")] board_halicarnassus,
        [Display(Name = "Posąg Zeusa w Olimpii")] board_zeus
    }
}