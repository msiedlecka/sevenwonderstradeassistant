﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using CardBuildWebApp.CardBuildServiceRef;
using CardBuildWebApp.Extras;
using CardBuildWebApp.Models;
using CardBuildWebApp.ViewModels;

namespace CardBuildWebApp.Controllers
{
    public class HomeController : Controller
    {
        private static readonly string allDataKey = "AllData";

        private TotalViewModel Total { get; set; }

        public ActionResult Index()
        {
            if (Total == null)
            {
                Total = new TotalViewModel(false);
                CreateInitialState();
                Session.Add(allDataKey, Total);
            }
            return View(Total);
        }

        [HttpPost]
        public async Task<ActionResult> CheckBuildPossibility(TotalViewModel totalVM)
        {
            Total = totalVM;
            if (ModelState.IsValid)
            {
                CardBuildServiceClient client = new CardBuildServiceClient();
                PlayerSimpleData[] players = GetPlayers().ToArray();
                string[] merchandise = Total.RequiredMerchandises.ChosenItems.Select(m => m.ToString()).ToArray();
                BuildPossibilityData data = await client.CheckCardBuildPossibilityAsync(players, merchandise);
                Total.RequiredMerchandises.BuildPossibility = data.CardCheckUsability;
                Session[allDataKey] = Total;
            }
            return PartialView("Index", Total);
        }

        [HttpPost]
        public ActionResult AddMerchandiseToPlayer(MerchandiseViewModel model)
        {
            TotalViewModel total = Session[allDataKey] as TotalViewModel;
            MerchandiseViewModel mvm = total.Players[model.PlayerIndex].Merchandises;
            mvm.ItemToAdd = model.ItemToAdd;
            UpdateProcessable(mvm);
            Session[allDataKey] = total;
            RestorePlayerPrefix(model, model.PlayerIndex, "Merchandises");
            return PartialView(Utils.GetPartialViewNameFor(mvm, "_MerchandisesViewPartial"), mvm);
        }

        [HttpPost]
        public ActionResult AddAlternativeToPlayer(MerchandiseAlternativeViewModel model)
        {
            TotalViewModel total = Session[allDataKey] as TotalViewModel;
            MerchandiseAlternativeViewModel mavm = total.Players[model.PlayerIndex].Alternatives;
            mavm.ItemToAdd = model.ItemToAdd;
            UpdateProcessable(mavm);
            Session[allDataKey] = total;
            RestorePlayerPrefix(model, model.PlayerIndex, "Alternatives");
            return PartialView(Utils.GetPartialViewNameFor(mavm, "_AlternativesViewPartial"), mavm);
        }

        [HttpPost]
        public ActionResult AddTradeBonusToPlayer(TradeBonusViewModel model)
        {
            TotalViewModel total = Session[allDataKey] as TotalViewModel;
            TradeBonusViewModel tbvm = total.Players[model.PlayerIndex].Bonuses;
            tbvm.ItemToAdd = model.ItemToAdd;
            UpdateProcessable(tbvm);
            Session[allDataKey] = total;
            RestorePlayerPrefix(model, model.PlayerIndex, "Bonuses");
            return PartialView(Utils.GetPartialViewNameFor(tbvm, "_TradeBonusesViewPartial"), tbvm);
        }

        [HttpPost]
        public ActionResult AddRequiredMerchandise(RequiredMerchandiseViewModel model)
        {
            TotalViewModel total = Session[allDataKey] as TotalViewModel;
            RequiredMerchandiseViewModel rmvm = total.RequiredMerchandises;
            rmvm.ItemToAdd = model.ItemToAdd;
            UpdateProcessable(rmvm);
            Session[allDataKey] = total;
            return PartialView(Utils.GetPartialViewNameFor(rmvm, "_RequiredMerchandisesViewPartial"), rmvm);
        }

        [HttpPost]
        public ActionResult SetBoardForPlayer(BoardViewModel model)
        {
            TotalViewModel total = Session[allDataKey] as TotalViewModel;
            BoardViewModel bvm = total.Players[model.PlayerIndex].Board;
            bvm.NewPlayerBoard = model.NewPlayerBoard;
            UpdateProcessable(bvm);
            Session[allDataKey] = total;
            RestorePlayerPrefix(model, model.PlayerIndex, "Board");
            return PartialView(Utils.GetPartialViewNameFor(bvm, "_BoardViewPartial"), bvm);
        }

        [HttpPost]
        public ActionResult SetEditModeTotal(TotalViewModel model, bool isEditMode)
        {
            TotalViewModel total = Session[allDataKey] as TotalViewModel;
            total.SetEditMode(isEditMode);
            total.RequiredMerchandises.BuildPossibility = BuildPossibilityType.NOT_DETERMINED;
            Session[allDataKey] = total;
            return PartialView(Utils.GetPartialViewNameFor(total, "Index"), total);
        }

        [HttpGet]
        public JsonResult DownloadCards()
        {
            return Json(CardsStorageClient.GetAllCardNames(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMerchandiseForCard(RequiredMerchandiseViewModel model)
        {
            TotalViewModel total = Session[allDataKey] as TotalViewModel;
            RequiredMerchandiseViewModel rmvm = total.RequiredMerchandises;
            List<Merchandise> merchandisesForCard = CardsStorageClient.GetMerchandiseForCard(model.CardName);
            rmvm.Clear();
            merchandisesForCard.ForEach(m => rmvm.Add(m));
            rmvm.BuildPossibility = BuildPossibilityType.NOT_DETERMINED;
            return PartialView(Utils.GetPartialViewNameFor(rmvm, "_RequiredMerchandisesViewPartial"), rmvm);
        }

        [HttpPost]
        public ActionResult SetRandomBoardForPlayer(int playerIndex)
        {
            TotalViewModel total = Session[allDataKey] as TotalViewModel;
            BoardViewModel model = total.Players[playerIndex].Board;
            Boards randomBoard = PlayerParamsGeneratorClient.GetRandomBoard();
            model.SetBoard(randomBoard);
            Session[allDataKey] = total;
            RestorePlayerPrefix(model, playerIndex, "Board");
            return PartialView(Utils.GetPartialViewNameFor(model, "_BoardViewPartial"), model);
        }

        [HttpPost]
        public ActionResult SetRandomCoinsForPlayer(int playerIndex)
        {
            TotalViewModel total = Session[allDataKey] as TotalViewModel;
            PlayerViewModel model = total.Players[playerIndex];
            int randomCoins = PlayerParamsGeneratorClient.GetRandomCoins();
            model.Coins = randomCoins;
            Session[allDataKey] = total;
            RestorePlayerPrefix(model, playerIndex, null);
            return PartialView(Utils.GetPartialViewNameFor(model, "_PlayerViewPartial"), model);
        }

        [HttpPost]
        public ActionResult SetRandomMerchandiseForPlayer(int playerIndex)
        {
            TotalViewModel total = Session[allDataKey] as TotalViewModel;
            MerchandiseViewModel model = total.Players[playerIndex].Merchandises;
            model.Clear();
            List<Merchandise> randomMerchandise = PlayerParamsGeneratorClient.GetRandomMerchandise();
            randomMerchandise.ForEach(m => model.Add(m));
            Session[allDataKey] = total;
            RestorePlayerPrefix(model, playerIndex, "Merchandises");
            return PartialView(Utils.GetPartialViewNameFor(model, "_MerchandisesViewPartial"), model);
        }

        [HttpPost]
        public ActionResult SetRandomAlternativeForPlayer(int playerIndex)
        {
            TotalViewModel total = Session[allDataKey] as TotalViewModel;
            MerchandiseAlternativeViewModel model = total.Players[playerIndex].Alternatives;
            model.Clear();
            List<MerchandiseAlternative> randomAlternative = PlayerParamsGeneratorClient.GetRandomAlternative();
            randomAlternative.ForEach(alt => model.Add(alt));
            Session[allDataKey] = total;
            RestorePlayerPrefix(model, playerIndex, "Alternatives");
            return PartialView(Utils.GetPartialViewNameFor(model, "_AlternativesViewPartial"), model);
        }

        [HttpPost]
        public ActionResult SetRandomTradeBonusForPlayer(int playerIndex)
        {
            TotalViewModel total = Session[allDataKey] as TotalViewModel;
            TradeBonusViewModel model = total.Players[playerIndex].Bonuses;
            model.Clear();
            List<TradeBonuses> randomBonuses = PlayerParamsGeneratorClient.GetRandomTradeBonuses();
            randomBonuses.ForEach(bonus => model.Add(bonus));
            Session[allDataKey] = total;
            RestorePlayerPrefix(model, playerIndex, "Bonuses");
            return PartialView(Utils.GetPartialViewNameFor(model, "_TradeBonusesViewPartial"), model);
        }

        private void RestorePlayerPrefix(Editable editable, int playerIndex, string sectionName)
        {
            if (!editable.IsInEditMode)
                ViewData.TemplateInfo.HtmlFieldPrefix = string.Format("Players[{0}]{1}", playerIndex,
                    !string.IsNullOrEmpty(sectionName) ? "." + sectionName : "");
        }

        private void UpdateProcessable(IProcessChanges processable)
        {
            if (ModelState.IsValid)
                processable.ProcessChanges();
        }

        private void CreateInitialState()
        {
            Total.Players.Clear();
            Total.Players.Add(GetRandomPlayer("left", 0, "Lewy gracz"));
            Total.Players.Add(GetRandomPlayer("main", 1, "Ty"));
            Total.Players.Add(GetRandomPlayer("right", 2, "Prawy gracz"));
            List<Merchandise> requiredMerchandises = PlayerParamsGeneratorClient.GetRandomMerchandise();
            requiredMerchandises.ForEach(m => Total.RequiredMerchandises.Add(m));
        }

        private PlayerViewModel GetRandomPlayer(string id, int index, string name)
        {
            return new PlayerViewModel(id, index)
            {
                Name = name,
                Board = new BoardViewModel(PlayerParamsGeneratorClient.GetRandomBoard(), index),
                Coins = PlayerParamsGeneratorClient.GetRandomCoins(),
                Merchandises =
                    new MerchandiseViewModel(index, PlayerParamsGeneratorClient.GetRandomMerchandise().ToArray()),
                Alternatives =
                    new MerchandiseAlternativeViewModel(index,
                        PlayerParamsGeneratorClient.GetRandomAlternative().ToArray()),
                Bonuses = new TradeBonusViewModel(index, PlayerParamsGeneratorClient.GetRandomTradeBonuses().ToArray())
            };
        }

        private List<PlayerSimpleData> GetPlayers()
        {
            IEnumerable<Player> players = Total.Players.Select(pvm => pvm.ToPlayer());
            return players.Select(p => p.ToPlayerSimpleData()).ToList();
        }
    }
}