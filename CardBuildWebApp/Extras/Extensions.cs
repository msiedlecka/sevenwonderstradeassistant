﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace ExtensionMethods
{
    public static class Extensions
    {
        public static string GetDescription(this Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    DescriptionAttribute attr =
                        Attribute.GetCustomAttribute(field,
                            typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attr != null)
                        return attr.Description;
                }
            }
            return null;
        }

        // http://stackoverflow.com/questions/29808573/getting-the-values-from-a-nested-complex-object-that-is-passed-to-a-partial-view
        public static MvcHtmlString PartialFor<TModel, TProperty>(this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> expression, string partialViewName)
        {
            string name = ExpressionHelper.GetExpressionText(expression);
            object model = ModelMetadata.FromLambdaExpression(expression, helper.ViewData).Model;
            string parentPrefix = helper.ViewData.TemplateInfo.HtmlFieldPrefix ?? "";

            if (parentPrefix != "")
                parentPrefix += ".";

            var viewData = new ViewDataDictionary(helper.ViewData)
            {
                TemplateInfo = new TemplateInfo {HtmlFieldPrefix = parentPrefix + name}
            };

            return helper.Partial(partialViewName, model, viewData);
        }

        // very similar to GetDescription(...), but retrieves the value of a bit different attribute 
        // (DisplayAttribute instead of DescriptionAttribute)
        public static string DisplayName(this object obj)
        {
            FieldInfo fieldInfo = obj.GetType().GetField(obj.ToString());
            if (fieldInfo != null)
            {
                var attributes = (DisplayAttribute[]) fieldInfo.GetCustomAttributes(typeof(DisplayAttribute), false);
                return attributes.Length > 0 ? attributes[0].Name : obj.ToString();
            }
            return obj.ToString();
        }
    }
}