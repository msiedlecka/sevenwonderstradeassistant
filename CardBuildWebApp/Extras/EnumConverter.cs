﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtensionMethods;

namespace CardBuildWebApp.Extras
{
    // simplified as I don't need a converter to cooperate with the binding system 
    // (it's just for me to get enum values from strings that I obtain from service)
    public class EnumConverter<T> where T : struct, IFormattable, IConvertible
        // constraints as close to Enum as possible
    {
        public static Dictionary<string, T> MapLabelsToObjects()
        {
            return MapToObjects(m => m.DisplayName());
        }

        public static Dictionary<string, T> MapStringValuesToObjects()
        {
            return MapToObjects(m => m.ToString());
        }

        private static Dictionary<string, T> MapToObjects(Func<T, string> labelCreator)
        {
            Dictionary<string, T> map = new Dictionary<string, T>();
            IEnumerable<T> allValues = Enum.GetValues(typeof(T)).Cast<T>();
            foreach (T value in allValues)
                map.Add(labelCreator.Invoke(value), value);
            return map;
        }

        // from string to (T)
        public T ConvertFrom(string value)
        {
            List<T> merchandises = new List<T>();
            Dictionary<string, T> map = MapStringValuesToObjects();
            T result;
            map.TryGetValue(value, out result);
            return result;
        }
    }
}